// WiFiPass.cpp : Defines the entry point for the console application.
//

#include "ParamEncrypt.h"

#define IN_LEN          64

char  in_buf[IN_LEN+1] ;


static __inline int digit(char c)
   {
   if (c < '0' || c > '9') return -1 ;
   return (c - '0') ;
   }


int GetNum(char *p)
   {
   int   d, v ;
      
   d = digit(*p++) ;
   if (d < 0) return -1 ;
   while (1)
      {
      v = digit(*p++) ;
      if (v < 0) break ;
      d = d*10 + v ;
      } ;
   return d ;
   }

typedef union
   {
   int  iv ;
   BYTE bv[4] ;
   } KEY ;


int main(int argc, char *argv[])
   {
   size_t   len, i, out_len, tot_len ;
   HGLOBAL  hMem ;
   char    *lpb ;
   int      v ;
   DWORD    AgencyID, ProductID, ProductDesig, Status ;
   KEY      keyVal ;
   BYTE     key ;

   printf("Agency ID (1-4095): ") ;
   scanf_s("%s", in_buf, IN_LEN) ;
   v = GetNum(in_buf) ;
   if (v < 1 || v > 4095)
      {
      printf("Invalid Agency ID. Must be 1-4095\r\n") ;
      goto EXIT ;
      } ;
   AgencyID = v ;

   printf("Product ID (0-7): ") ;
   scanf_s("%s", in_buf, IN_LEN) ;
   v = GetNum(in_buf) ;
   if (v < 0 || v > 7)
      {
      printf("Invalid Product ID. Must be 0-7\r\n") ;
      goto EXIT ;
      } ;
   ProductID = v ;

   printf("Product designator (0-248): ") ;
   scanf_s("%s", in_buf, IN_LEN) ;
   v = GetNum(in_buf) ;
   if (v < 0 || v > 248)
      {
      printf("Invalid Product designator. Must be 0-248\r\n") ;
      goto EXIT ;
      } ;
   ProductDesig = v ;

   printf("Account status (0 or 1): ") ;
   scanf_s("%s", in_buf, IN_LEN) ;
   v = GetNum(in_buf) ;
   if (v < 0 || v > 1)
      {
      printf("Invalid Account status. Must be 0-1\r\n") ;
      goto EXIT ;
      } ;
   Status = v ;

   srand(GetTickCount()) ;
   keyVal.iv = rand() ;
   key = keyVal.bv[0] ^ keyVal.bv[2] ^ keyVal.bv[1] ^ keyVal.bv[3] ;

   in_buf[0] = key ;
   len = 1 + sprintf_s(&in_buf[1], IN_LEN-2, "%04u %u %03u %u", AgencyID, ProductID, ProductDesig, Status) ;

   tot_len = len*2 + 1 ;
   hMem = GlobalAlloc(GMEM_MOVEABLE, tot_len) ;
   lpb = (char*)GlobalLock(hMem) ;

   out_len = sprintf_s(lpb, tot_len, "%02X", key) ;
   for (i = 1; i < len; i++)
      {
      out_len += sprintf_s(&lpb[out_len], tot_len - out_len, "%02X", 0xFF & (key ^ in_buf[i] ^ (0x40 + i))) ;
      } ;
   

   printf("Encoded:\r\n%s\r\nUse Paste command to copy\r\n", lpb) ;

   GlobalUnlock(hMem) ;
   OpenClipboard(NULL) ;
   EmptyClipboard() ;
   SetClipboardData(CF_TEXT, hMem) ;
   CloseClipboard() ;

   EXIT:
   printf("Hit Enter key to continue...") ;
   scanf_s("%2c", in_buf, IN_LEN) ;
	return 0 ;
   }
